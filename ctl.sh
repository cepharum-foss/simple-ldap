#!/bin/sh

[ -f /run/secrets/simple-ldap.config.tgz ] && tar xzf /run/secrets/simple-ldap.config.tgz -C /config

for name in "${DATA_FILENAME:-}" ldap-db.json simple-ldap-entries.json entries.json; do
	if [ -n "$name" -a -f "/config/$name" ]; then
		npm run start "/config/$name"
		exit $?
	fi
done
