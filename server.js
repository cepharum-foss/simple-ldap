const File = require( "fs" );
const Crypto = require( "crypto" );

const LDAP = require( "ldapjs" );

const { parseDN } = LDAP;

// ----------------------------------------------------------------------------

const organization = process.env.SIMPLE_LDAP_ORGANIZATION || "foo";

const RootNode = {
	dn: "o=" + organization,
	attributes: {
		o: organization,
		objectclass: [ "organization", "top" ],
	},
};

// ----------------------------------------------------------------------------

const RootDN = parseDN( RootNode.dn );

return File.promises.readFile( process.argv[2] || process.env.SIMPLE_LDAP_DB_FILE || "ldap-db.json", "utf8" )
	.then( fileContent => JSON.parse( fileContent ) )
	.then( source => source.map( qualifyRecord ) )
	.then( db => {
		const server = LDAP.createServer();

		const cookies = new Map();

		setInterval( () => {
			const now = Date.now();
			let count = 0;

			for ( const [ key, cookie ] of cookies ) {
				if ( now - cookie.used > 60000 ) {
					cookies.delete( key );
					count++;
				}
			}

			if ( count > 0 ) {
				console.log( "removed %d cookie(s) related to previous incomplete search requests", count );
			}
		}, 10000 );

		server.search( RootNode.dn, ( req, res, next ) => {
			console.log( "handling search for %s in %s with scope %s", req.filter.toString(), req.dn.toString(), req.scope );

			let cookie = { offset: 0, limit: Infinity, total: 0, used: Date.now() };
			const pager = req.controls.find( control => control.type === LDAP.PagedResultsControl.OID );

			if ( pager ) {
				let mode;

				if ( pager.value.cookie.length > 0 ) {
					const raw  = pager.value.cookie;
					const name = raw.toString( "base64" );

					if ( cookies.has( name ) ) {
						mode = "existing";

						cookie = cookies.get( name );
						cookie.used = Date.now();
					}
				}
				
				if ( !mode ) {
					while ( true ) {
						const raw = Crypto.randomBytes( 16 );
						const name = raw.toString( "base64" );

						if ( !cookies.has( name ) ) {
							cookie.octets = raw;
							cookies.set( name, cookie );
							break;
						}
					}

					mode = "new";
				}

				cookie.limit = pager.value.size;

				console.log( "got pager accessing %s cookie %s to fetch %d match(es) skipping %d match(es)", 
					mode, cookie.octets.toString( "base64" ), cookie.limit, cookie.offset );
			}

			
			if ( req.dn.equals( RootDN ) ) {
				searchTree( db, cookie.offset, cookie.limit, req, res, onSentMatches );
			} else if ( req.dn.parent().equals( RootDN ) ) {
				fetchLeaf( db, cookie.offset, cookie.limit, req, res, onSentMatches );
			} else {
				onSentMatches( Infinity );
			}

			/**
			 * Commonly finishes end of current search request.
			 * 
			 * @param {int} numSentItems number of items sent in current response
			 * @param {int} numTotalMatches (estimated) total number of matching tems, omit if unknown
			 * @returns {void}
			 */
			function onSentMatches( numSentItems, numTotalMatches = Infinity ) {
				if ( pager && cookie ) {
					console.log( "having sent %d match(es) out of %d total match(es)", numSentItems, numTotalMatches );

					cookie.offset += numSentItems;

					if ( cookie.offset >= numTotalMatches ) {
						console.log( "detected end of paged queries" );

						res.controls.push( new LDAP.PagedResultsControl( { value: {
							size: numTotalMatches === Infinity ? 0 : numTotalMatches,
							cookie: Buffer.alloc( 0 ),
						} } ) );

						cookies.delete( cookie.octets.toString( "base64" ) );
					} else {
						console.log( "respond with control indicating further matches available" );

						res.controls.push( new LDAP.PagedResultsControl( { value: {
							size: numTotalMatches === Infinity ? 0 : numTotalMatches,
							cookie: cookie.octets,
						} } ) );
					}
				}

				res.end();
			}
		} );
		
		server.listen( 389, () => {
			console.log( "ldapjs listening at " + server.url );
		} );
	} )
	.catch( error => {
		console.error( "running simple LDAP server failed: " + error.stack );
	} );


/**
 * Handles request for searching nodes in scope of this directory's root DN.
 * 
 * @param {Array<object>} db list of nodes to expose in LDAP
 * @param {int} offset number of matches to skip
 * @param {int} limit number of matches to deliver at most
 * @param {*} req request descriptor
 * @param {*} res response manager
 * @param {function(numSent:int, numTotal:int=):void} done callback to invoke when done
 * @returns {void}
 */
function searchTree( db, offset, limit, req, res, done ) {
	let _offset = offset;
	let count = 0;

	switch ( req.scope ) {
		case "base" :
		case "sub" : {
			const attributes = Object.assign( {}, RootNode.attributes );

			if ( req.filter.matches( attributes ) ) {
				if ( _offset-- < 1 ) {
					res.send( { dn: RootNode.dn, attributes } );

					if ( ++count >= limit ) {
						done( count );
						return;
					};
				}
			}
		}
	}

	switch ( req.scope ) {
		case "one" :
		case "sub" : {
			const numItems = db.length;

			for ( let i = 0; i < numItems; i++ ) {
				const item = db[i];
				const attributes = Object.assign( {}, item.attributes );

				if ( req.filter.matches( attributes ) ) {
					if ( _offset-- < 1 ) {
						res.send( { dn: item.dn, attributes } );

						if ( ++count >= limit ) {
							done( count );
							return;
						}
					}
				}
			}
		}
	}

	done( Infinity );
}

/**
 * Handles request for fetching single leaf matching given DN.
 * 
 * @param {Array<object>} db list of nodes to expose in LDAP
 * @param {int} offset number of matches to skip
 * @param {int} limit number of matches to deliver at most
 * @param {*} req request descriptor
 * @param {*} res response manager
 * @param {function(numSent:int, numTotal:int=):void} done callback to invoke when done
 * @returns {void}
 */
function fetchLeaf( db, offset, limit, req, res, done ) {
	let _offset = offset;
	let count = 0;

	switch ( req.scope ) {
		case "base" :
		case "sub" : {
			const numItems = db.length;

			for ( let i = 0; i < numItems; i++ ) {
				const item = db[i];
				const attributes = Object.assign( {}, item.attributes );

				if ( req.dn.equals( item.dn ) && req.filter.matches( attributes ) ) {
					if ( _offset-- < 1 ) {
						res.send( { dn: item.dn, attributes } );

						if ( ++count >= limit ) {
							done( count );
							return;
						}
					}
				}
			}
		}
	}

	done( Infinity );
}

/**
 * Converts raw record read from database file into object suitable for 
 * describing a single LDAP entry in context of ldapjs.
 * 
 * @param {object} record record to be qualified for use with ldapjs
 * @returns {{dn:string, attributes:object}} qualified record
 */
function qualifyRecord( record ) {
	const dn = "cn=" + record.cn + "," + RootDN.toString();

	const hash = Crypto.createHash( "sha256" );
	hash.update( dn );
	const uuid = hash.digest().toString( "hex" ).substr( 0, 32 );

	const copy = {
		dn: parseDN( dn ),
		attributes: {
			objectclass: [ "top", "inetorgperson" ],
		},
	};

	const dest = copy.attributes;
	const names = Object.keys( record );
	const numNames = names.length;

	for ( let i = 0; i < numNames; i++ ) {
		const name = names[i];
		const value = record[name];

		const match = name.match( /^([^:;.]+)(?:[:;.]s(sha\d*))$/i );
		const hash = match ? match[2].toLowerCase() : null;

		const destName = match ? match[1] : name;
		dest[destName] = hash ? ssha( value, null, hash === "sha" || hash === "sha1" ? null : hash ) : value;
	}

	if ( !dest.entryUUID ) {
		dest.entryUUID = uuid.replace( /^(.{8})(.{4})(.{4})(.{4})(.{12})$/, "$1-$2-$3-$4-$5" );
	}

	console.log( "adding record %s with %j", dn, dest );

	return copy;
}

/**
 * Derives UUID from hash created on provided string.
 * 
 * @param {string} string input string to be hashed for deriving UUID from
 * @returns {string} derived UUID
 */
function uuidFromHash( string ) {
	const hash = Crypto.createHash( "sha1" );

	hash.update( string );

	return hash.digest( "hex" ).replace( /^(.{8})(.{4})(.{4})(.{4})(.{12})$/, "$1-$2-$3-$4-$5" );
}

/**
 * Hashes some cleartext information with some salt.
 * 
 * @param {string|buffer} cleartext some cleartext information to be hashed
 * @param {string|buffer} salt salt to use, omit for random one (suggested unless verifying)
 * @param {string} algorithm name of digest to use
 */
function ssha( cleartext, salt = null, algorithm = "sha1" ) {
	const hash = Crypto.createHash( algorithm || "sha1" );
	const _salt = salt || Crypto.randomBytes( 16 );

	hash.update( cleartext );
	hash.update( _salt );

	return `{S${( algorithm || "SHA" ).toUpperCase()}}` + Buffer.concat( [ hash.digest(), _salt ] ).toString( "base64" );
}
