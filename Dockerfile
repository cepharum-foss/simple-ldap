FROM node:lts-alpine

COPY ctl.sh server.js package.json package-lock.json /app/
RUN cd /app && npm ci && mkdir /config && echo "[]" >/config/entries.json

VOLUME /config
EXPOSE 389

WORKDIR /app
ENTRYPOINT [ "/bin/ash", "/app/ctl.sh" ]